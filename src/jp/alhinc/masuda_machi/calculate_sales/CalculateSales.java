package jp.alhinc.masuda_machi.calculate_sales;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {


		HashMap <String, String> nameMap = new HashMap <String, String> ();
		HashMap <String, Long> salesNameMap = new HashMap <String, Long> ();


		if (args.length != 1) {
			System.out.println ("予期せぬエラーが発生しました") ;
			return;
		}


		if (! Input (args[0], "branch.lst", nameMap,salesNameMap, "支店", "\\d{3}")) {
			return;
		}



		ArrayList <String>  salefile = new ArrayList <String> ();
		ArrayList <Integer>  salefileinteger = new ArrayList <Integer> ();

		File dir = new File (args[0]);
		File [] files = dir.listFiles ();

		for (int i = 0 ; i< files.length ; i++) {
			File file = files[i];

			if (file.isFile() && file.getName().matches("\\d{8}.rcd") ) {
				salefile.add(file.getName());
			}


		}


		for (int i = 0 ; i< salefile.size() ; i++) {
			String number = salefile.get(i);
			String number8 = number.substring(0,8);

			int numbers8 = Integer.parseInt(number8);
			salefileinteger.add(numbers8);
		}

		for (int i = 0 ; i< salefile.size() -1 ; i++) {
			if (salefileinteger.get(i + 1) - salefileinteger.get(i) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

			//売り上げのファイルを出す
		BufferedReader br = null;
		for (int i = 0 ; i <salefile.size() ; i++){
			try{
				File file = new File(args[0], salefile.get(i));
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);

				//ファイルの中身を表示
				String line;
				ArrayList <String> sales = new ArrayList <String> ();
				while ((line=br.readLine()) != null) {
					sales.add(line);
				}
				if (sales.size() != 2) {
					System.out.println(salefile.get(i) + "のフォーマットが不正です");
					return;
				}



				if (! sales.get(1).matches("^[0-9]+")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}



				if (! salesNameMap.containsKey(sales.get(0))) {
					System.out.println(salefile.get(i) + "の支店コードが不正です");
					return;
				}



				String code = sales.get(0);
				//売り上げの　20000の分を数値に変換してあげた
				Long salesLong = Long.parseLong(sales.get(1));
				Long saleSum = salesLong + salesNameMap.get(code);

				if (saleSum > 9999999999L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				salesNameMap.put(code, saleSum);


			}catch(IOException ela) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}
			finally{
				if (br != null) {
					try {
						br.close();
					}catch(IOException ela) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
		         }
			 }

		}
		if (! Output(args[0], "branch.out", nameMap, salesNameMap)) {
			return;
		}
	}

	public static boolean Input(String path, String fileList,
		HashMap <String,String> nameMap, HashMap <String, Long> salesNameMap,
		String saleName, String number) {

			BufferedReader br = null;

			try{
				File file = new File(path, fileList);

				if (!file.exists()) {
					System.out.println(saleName + "定義ファイルが存在しません");
					return false;
				}
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);

				String line;
				while ((line = br.readLine()) != null) {

					String[] items = line. split(",");

					if (!items[0].matches(number)) {
						System.out.println(saleName + "定義ファイルのフォーマットが不正です");
						return false;
					}
					if (items.length != 2) {
						System.out.println(saleName + "定義ファイルのフォーマットが不正です");
						return false;
					}


					nameMap.put(items[0], items[1]);
					salesNameMap.put(items[0], 0L);
				}

			}catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
			finally{
				if (br != null) {
					try{
						br.close();
					}catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return false;
					}
				}
			}
			return true;
		}


	 public static boolean Output(String path, String fileOut,
			 HashMap <String, String> nameMap, HashMap <String, Long> salesNameMap){

		 BufferedWriter bw = null;

		try{
			File file = new File(path, fileOut);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for (Map.Entry <String, String> entry : nameMap.entrySet()) {
				String a = (entry.getKey());
				bw.write(entry.getKey() + "," + entry.getValue() + "," + salesNameMap.get(a));
				bw.newLine();
			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}
		finally{
			if (bw != null) {
				try{
					bw.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
				return false;
				}
			}
		}
		return true;
	 }
}

